const express = require("express");
const fs = require("fs/promises");
const path = require("path");

const router = express.Router();
router.use(express.json());

router.post("/", (req, res, next) => {
  const { directoryName, files } = req.body;

  function filenamesIncludeSLashOrNot() {
    const filenamesThatIncludeSlash = files.filter((file) => {
      return file.includes("/");
    });
    if (filenamesThatIncludeSlash.length > 0) {
      // if there are filenames that include /
      return true;
    } else {
      return false;
    }
  }

  function filenamesAreStringOrNot() {
    const filenamesNotString = files.filter((file) => {
      return typeof file !== "string";
    });
    if (filenamesNotString.length > 0) {
      // if any filename is found  not string
      return true;
    } else {
      return false;
    }
  }

  if (req.headers["content-type"] !== "application/json") {
    next({
      statusCode: 400,
      message: "content-type not found in json format",
    });
  } else if (typeof directoryName !== "string" || !Array.isArray(files)) {
    next({
      statusCode: 400,
      message:
        "directory name should be in string format and files should be in an array",
    });
  } else if (directoryName.includes("/")) {
    next({
      statusCode: 400,
      message: "directory name should not include /",
    });
  } else if (directoryName.trim().length === 0) {
    next({
      statusCode: 400,
      message: "directory name cannot be empty",
    });
  } else if (files.length === 0) {
    next({
      statusCode: 400,
      message: "Please mention the files to be created",
    });
  } else if (filenamesAreStringOrNot()) {
    next({
      statusCode: 400,
      message: "filenames in files array must be string",
    });
  } else if (filenamesIncludeSLashOrNot()) {
    next({
      statusCode: 400,
      message: "Filenames include /, do not include /",
    });
  } else {
    const directoryPath = path.join(__dirname, directoryName);
    fs.mkdir(directoryPath, { recursive: true })
      .then(() => {
        const promises = files.map((filename) => {
          const filePath = path.join(directoryPath, `${filename}.json`);
          const data = JSON.stringify({ name: filename + "hi" });
          return fs.writeFile(filePath, data);
        });
        return Promise.all(promises);
      })
      .then(() => {
        res.status(200).json({ message: "Files created successfully" });
      })
      .catch((error) => {
        console.error(error);
        next({
          statusCode: 500,
          message: "Error creating files",
        });
      });
  }
});

module.exports = router;
