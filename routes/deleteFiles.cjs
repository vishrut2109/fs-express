const express = require("express");
const fs = require("fs/promises");

const router = express.Router();
router.use(express.json());

router.delete("/", (req, res, next) => {
  const { directoryName, files } = req.body;

  const directoryPath = `./routes/${directoryName}`;

  function filenamesIncludeSLashOrNot() {
    const filenamesThatIncludeSlash = files.filter((file) => {
      return file.includes("/");
    });
    if (filenamesThatIncludeSlash.length > 0) {
      // if there are  filenames that include /
      return true;
    } else {
      return false;
    }
  }

  function filenamesAreStringOrNot() {
    const filenamesNotString = files.filter((file) => {
      return typeof file !== "string";
    });
    if (filenamesNotString.length > 0) {
      return true;
    } else {
      return false;
    }
  }

  if (req.headers["content-type"] !== "application/json") {
    next({
      statusCode: 400,
      message: "content-type not found in json format ",
    });
  } else if (typeof directoryName !== "string" || !Array.isArray(files)) {
    next({
      statusCode: 400,
      message:
        "directory name should be in string format and files should be an array",
    });
  } else if (directoryName.includes("/")) {
    next({
      statusCode: 400,
      message: "directory name should not include /",
    });
  } else if (directoryName.trim().length === 0) {
    next({
      statusCode: 400,
      message: "directory name cannot be empty",
    });
  } else if (files.length === 0) {
    next({
      statusCode: 400,
      message: "Please mention the files to be deleted ",
    });
  } else if (filenamesAreStringOrNot()) {
    next({
      statusCode: 400,
      message: "filenames in files array must be string",
    });
  } else if (filenamesIncludeSLashOrNot()) {
    next({
      statusCode: 400,
      message: `Filenames  include / , Do not include /`,
    });
  } else {
    Promise.all(
      files.map((fileName) => {
        const filePath = `${directoryPath}/${fileName}`;
        return fs.unlink(filePath);
      })
    )
      .then(() => {
        res.status(200).json({ message: "Files deleted successfully" });
      })
      .catch((error) => {
        console.error(error);

        if (error.code === "ENOENT") {
          next({
            statusCode: 400,
            message: `the file you are trying to delete does not exist !`,
          });
        } else {
          next({
            statusCode: 500,
            message: "Error deleting files",
          });
        }
      });
  }
});

module.exports = router;
