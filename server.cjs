const express = require("express");
const errorHandler = require("./errorHandler.cjs");

const app = express();
app.use(express.json());

const createDirectory = require("./routes/createDirectory.cjs");
const deleteFiles = require("./routes/deleteFiles.cjs");

app.use("/createDirectory", createDirectory);

app.use("/deleteFiles", deleteFiles);

app.all("*", (request, response) => {
  response.status(404).json({
    404: "Not found",
  });
});

// error handler
app.use(errorHandler);

app.listen(8000, () => {
  console.log("Server is listening on port 8000");
});
