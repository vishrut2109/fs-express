function errorHandler(err, req, res, next) {
  if (err.type === "entity.parse.failed") {
    res
      .status(err.statusCode)
      .json({
        message: "Body format in invalid JSON ",
      })
      .end();
  } else {
    console.error(err);
    res.status(err.statusCode).json(err).end();
  }
}

module.exports = errorHandler;
